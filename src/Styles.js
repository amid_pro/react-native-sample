import {StyleSheet, Dimensions} from 'react-native';

const dimensions = Dimensions.get('window');

export default StyleSheet.create({

	content: {
		flex: 1,
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingTop: 30,
		backgroundColor: '#1e90ff',
		position: 'relative',
	},

	header: {
		flex: 1,
		paddingTop: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
		position: 'absolute',
		top: 0,
		left: 20,
		right: 20,
	},

	header_text: {
		color: '#ffffff',
	},


	info_page: {
		flex: 1,
		backgroundColor: '#fff',
		padding: 10,
		margin: 10,
		borderRadius: 4,
	},

	item: {
		flex: 1,
		backgroundColor: '#ffffff',
		margin: 20,
	},

	item_title: {
		textAlign: 'center',
		color: '#ccc',
		marginTop: 5,
	},

	item_price: {
		textAlign: 'center',
		color: '#ff0000',
		marginTop: 5,
	},

	item_image: {
		flex: 1,
		resizeMode: "contain",
	},

	item_button: {
		backgroundColor: '#ff0000',
	},

	item_button_text: {
		color: '#ffffff',
	},

	order_label: {
		marginLeft: 0,
		marginBottom: 20,
	},

	order_button: {
		marginTop: 20,
	},

	order_button_text: {
		color: '#fff',
	},	

	order_item: {
		backgroundColor: '#CCCCFF',
		borderRadius: 4,
		padding: 10,
		marginBottom: 20,
	},

	order_buttons_line: {
		flexDirection: 'row',
		marginTop: 10,
		marginBottom: 10,
	},

	order_buttons_line_button: {
		marginRight: 10,
		height: 30,
		width: 30,
	},

	order_buttons_line_button_text: {
		color: '#fff',
		textAlign: 'center',
		width: '100%',
		fontSize: 20,
	},		

});