import React, {Component} from 'react';
import {View, Text, Dimensions} from 'react-native';
import { Spinner, Button } from 'native-base';
import Image from 'react-native-scalable-image';
import Styles from "./Styles";
import {getItem} from './Db';
import store from "./store/Store";


export default class Item extends Component {


    constructor(props){
        super(props)
        this.state = {
            content: null,
        }
    }


    addToCart(item_id){
      	store.dispatch({
              type: "ADD_ITEM",
           	  id: item_id
          });
    };


    componentDidMount(){

      	getItem(this.props.item_id).then((item) => {
        		this.setState({
          			item: item[0],
          			content: 'item'
        		})
      	})

    }


    render(){

      	const screen_width = Dimensions.get('window').width

        let content = <Spinner color='#ffffff' />

        if (this.state.content === 'item'){
            content = <View style={Styles.item}>
                        <Text style={Styles.item_title}>{this.state.item['title']}</Text>
                        <Text style={Styles.item_price}>{this.state.item['price']}</Text>
                        <Image width={screen_width - 40} style={Styles.item_image} source={{uri: this.state.item['image']}} />
                  		  <Button onPress={() => this.addToCart(this.state.item['id']) } block info small style={Styles.item_button}>
                  		  	<Text style={Styles.item_button_text}>Добавить в заказ</Text>
                  		  </Button>
                      </View>
        }

      	return(<View>{content}</View>);
    }

}