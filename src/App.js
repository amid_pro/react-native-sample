import React, {Component} from 'react';
import {View, Text, SectionList, Dimensions} from 'react-native';
import { Spinner } from 'native-base';
import Image from 'react-native-scalable-image';
import Styles from "./Styles";
import Contacts from './Contacts';
import Item from './Item';
import Cart from './Cart';
import {updateDb} from './Db';
import Count from "./Count";


export default class App extends Component {


    constructor(props){
        super(props)
        this.state = {
            content: null,
            items: [],
            item_id: null
        }
    }


    componentDidMount(){

        updateDb().then((items) => {

            let _items = [];
            Object.keys(items).map((index) => {
              _items[index] = [];
              _items[index]['title'] = items[index]['title'];
              _items[index]['data'] = [{
                id: items[index]['id'],
                title: items[index]['title'],
                price: items[index]['price'],
                image: items[index]['image'],
              }];

            });

            return _items;

        }).then((items) => {

            this.setState({
                content: 'items',
                items: items
            });

        });

    }

    render(){

        const screen_width = Dimensions.get('window').width;

        let content = <Spinner color='#ffffff' />

        const sectionItem = ({item, index}) => {
          return (
            <View style={Styles.item}>
              <Text style={Styles.item_title}>{item['title']}</Text>
              <Text style={Styles.item_price}>{item['price']}</Text>
              <Image onPress={() => this.setState({ content: 'item', item_id: item['id'] })} width={screen_width - 40} style={Styles.item_image} source={{uri: item['image']}} />
            </View>
        )};

        if (this.state.content === 'items'){
            content = <SectionList
                        renderItem = {sectionItem}
                        sections={this.state.items}
                        keyExtractor={(item, index) => item + index}
                      />
        }
        else if (this.state.content === 'contacts'){
            content = <Contacts />
        }
        else if (this.state.content === 'item'){
            content = <Item item_id={this.state.item_id} />
        }
        else if (this.state.content === 'cart'){
            content = <Cart />
        }

        return(<View style={Styles.content}>
                <View style={Styles.header}>
                  <Text style={Styles.header_text} onPress={() => {this.setState({ content: 'items' })}}>Главная</Text>
                  <Text style={Styles.header_text} onPress={() => {this.setState({ content: 'contacts' })}}>Контакты</Text>
                  <Text style={Styles.header_text} onPress={() => {this.setState({ content: 'cart' })}}>Заказ<Count /></Text>
                </View>
                {content}
              </View>)
    }

}