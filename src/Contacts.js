import React, {Component} from 'react';
import {View, Text} from 'react-native';
import { Content } from 'native-base';
import { getContacts } from './Db';
import Styles from "./Styles";


export default class Contacts extends Component {


    constructor(props){
        super(props);
        this.state = {
            data: {}
        };
    }


    componentDidMount(){
        this.setState({
            data: getContacts()
        });
    }


    render(){

      const contacts = Object.keys(this.state.data).map((index) => {
                          return <Text key={index}>{this.state.data[index]['name']} : {this.state.data[index]['value']}</Text>
                       });

      return(<Content style={Styles.info_page}><Text>Контакты</Text>
              {contacts}
            </Content>)
    }

}