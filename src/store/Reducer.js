const initialUserState = {
  count: 0, items: []
}


export default reducer = (state = initialUserState, action) => {

	switch (action.type) {

		case "ADD_ITEM":
			if (state['items'][action.id]){
				state['items'][action.id]++;
			}
			else {
				state['items'][action.id] = 1;
			}
			state['count']++;
			return state;

		case "REMOVE_ITEM":
			if (state['items'][action.id] > 1){
				state['items'][action.id]--;
			}
			else {
				delete state['items'][action.id];
			}
			state['count']--;
			return state;

		case "DOWN_COUNT":
		    state['count'] = 0;
		    state['items'] = [];
		    return state;

		default: return state;
	}
};
