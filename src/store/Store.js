import {createStore} from 'redux'
import reducer from './Reducer'

export default store = createStore(reducer);

