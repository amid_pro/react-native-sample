import React, {Component} from 'react';
import {View, Text} from 'react-native';
import { Content, Textarea, Form, Item, Input, Button } from 'native-base';
import Styles from "./Styles";
import Db from './Db';
import store from "./store/Store";


export default class Cart extends Component {


    constructor(props){
        super(props)
        this.state = {
            content: null,
            items: []
        }
    }

    componentDidMount(){
        this.setState({
            content: 'order',
            items: store.getState().items
        });
    }

    orderSend(){
        this.setState({
            content: 'success',
            items: []
        });
        store.dispatch({
            type: 'DOWN_COUNT'
        });
        this.forceUpdate();
    }

    plusItem(id){
        store.dispatch({
            type: "ADD_ITEM",
            id: id
        });
        this.forceUpdate();
    }

    minusItem(id){
        store.dispatch({
            type: "REMOVE_ITEM",
            id: id
        });
        this.forceUpdate();
    }

    render(){

        let content;

        if (this.state.content === 'order'){

          if (store.getState().count < 1){
              content = <Content style={Styles.info_page}><Text>Заказ пуст</Text></Content>
          }

          else {

            const items = Db.getOrderItems(Object.keys(this.state.items));
            let summ = 0;

            content = <View>
                      {Object.keys(items).map((index) => {

                        const id = items[index]['id'];
                        const count = this.state.items[id];
                        const item_summ = items[index]['price'] * count;
                        summ += item_summ;
                        
                        return  <View key={index} style={Styles.order_item}>
                                  <Text>{items[index]['title']} x {count} = {item_summ}</Text>
                                  <View style={Styles.order_buttons_line}>
                                    <Button onPress={() => this.plusItem(id)} style={Styles.order_buttons_line_button} small info>
                                      <Text style={Styles.order_buttons_line_button_text}>+</Text>
                                    </Button>
                                    <Button onPress={() => this.minusItem(id)} style={Styles.order_buttons_line_button} small warning>
                                      <Text style={Styles.order_buttons_line_button_text}>—</Text>
                                    </Button>
                                  </View>
                                </View>
                      })}

                      <Text>Сумма заказа: {summ}</Text>

                      <Form style={{ flex: 1, width: '100%' }}>
                        <Item style={Styles.order_label}>
                          <Input placeholder="Ваше имя" />
                        </Item>

                        <Item style={Styles.order_label}>
                          <Input placeholder="Телефон или email" />
                        </Item>

                        <Textarea rowSpan={5} bordered placeholder="Пожелания к заказу" />

                        <Button onPress={() => this.orderSend()} small style={Styles.order_button} block>
                          <Text style={Styles.order_button_text}>Оформить заказ</Text>
                        </Button>

                      </Form>
                      </View>
          }
        }
        else if (this.state.content === 'success'){
            content = <Text>Заказ оформлен</Text>
        }

    	  return(<Content style={Styles.info_page}><Text>Заказ</Text>{content}</Content>);
    }

}