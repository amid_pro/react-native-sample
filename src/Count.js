import React, {Component} from 'react';
import {Text} from 'react-native';
import Styles from "./Styles";
import Db from "./Db";
import store from "./store/Store";


export default class Count extends Component {


	constructor(props){
		super(props);
		this.state = {
			count: 0
		};
	}


	componentDidMount(){
		store.subscribe(() => {
			this.setState({
				count: store.getState().count
			})
		});
	}


	render(){

		const counter = this.state.count > 0 ? (': ' + this.state.count) : null;

		return (<Text>{counter}</Text>);
	}

}