import Realm from 'realm';

const DATA_ADDRESS = 'http://test.loc';

class Item {}
Item.schema = {
  name: 'Item',
  properties: {
    id: 'int',
    title: 'string',
    description: {type: 'string', default: ''},
    price: {type: 'int', default: 0},
    image: {type: 'string', optional: true},
  }
};


class Contacts {}
Contacts.schema = {
  name: 'Contacts',
  properties: {
    name: 'string',
    value: 'string',
  }
};

const realm = new Realm({schema: [Item, Contacts]});

const deleteAll = () => {
	realm.write(() => {
		realm.delete(realm.objects('Item'));
		realm.delete(realm.objects('Contacts'));
	});
};

const addItems = (items) => {
  Object.keys(items).map((index) => {
		realm.write(() => {
			realm.create('Item', { id: items[index]['id'], title: items[index]['title'], description: items[index]['description'], price: items[index]['price'], image: items[index]['image'] });
		});
  });
};

const addContacts = (contacts) => {
	Object.keys(contacts).map((index) => {
		realm.write(() => {
			realm.create('Contacts', { name: contacts[index]['name'], value: contacts[index]['value'] });
		});
	});
};

module.exports = {

	DATA_ADDRESS: DATA_ADDRESS,

	getItems: () => {
		return realm.objects('Item');
	},

	getItem: (id) => {
		return new Promise((resolve, reject) => {
			const item = realm.objects('Item').filtered('id = "' + id + '"');
			if (item){
				resolve(item);
			}
			reject();
		});
	},

	getContacts: () => {
		return realm.objects('Contacts');
	},

	getOrderItems: (arr) => {
		const sql = "id = " + arr.join(' OR id = ');
		return realm.objects('Item').filtered(sql);
	},

	updateDb: async () => {

		const data = await fetch(DATA_ADDRESS).then((response) => {
			return response.json();
		});

		deleteAll();
		addItems(data['items']);
		addContacts(data['contacts']);

		return data['items'];
	},
}

realm.removeAllListeners();